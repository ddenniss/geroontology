Welcome to GeroOntology project!

Here you have links to all relevant project resources.

## Actual project resources
* GeroOntology - our chat in Telegram
* [Working plan](https://docs.google.com/document/d/1cdVyw239wLvRJb2neu30J_sgY6oC7nupLzvzkcaKKYU/edit#heading=h.pewuwmbj3lmx)
* The paper draft
* [Islands - ECM](https://docs.google.com/spreadsheets/d/1OTjfN_lqK-MyfgL3Pf-JuWBg77pnjVNStHWh7Bc8GTg/edit#gid=0)
* [Islands - Elastin](https://docs.google.com/document/d/1Uab-aTOVIQiMczgLsWGaPNXXmVW7EgAR6WrLtLgP99c/edit)
* [Ontology in Webprotege](https://webprotege.stanford.edu/#projects/64f29fb1-bf87-480c-b221-73e9928490b4)
* [Central files repository](https://gitlab.com/ddenniss/geroontology) - Resulting ontology files that will be published on BioPortal, and also this README

### Currently unused project resources
* [Jupyter charts](http://jupyter.msu-web.tech)
* [Trello tasks and project management](https://trello.com/b/Vku7K0Gk)
* [Miro mindmaps](https://miro.com/welcomeonboard/Nk1lQkVsSlZveU1DaW1GblpaVnhVaHRXTUhlNDFRT3piSEpoZkxKSk5jU3VVaWxqRDRYNklrQXFJdWxDdk10OXwzNDU4NzY0NTI5MDg0ODkzNDUy?share_link_id=685531029840) - collaborative board for charting mindmaps

# GeroOntology

GeroOntology is an ontology in OWL (Web Ontology Language). It describes concepts and their relations used in the research of aging / gerontology.

The ontology is useful in the tasks of automating knowledge extraction from the papers, biological databases integration, metabolism modelling and other cool stuff.

This repository contains various files for share use in the course of ontology development, including resulting .ttl files of the ontology itself.
