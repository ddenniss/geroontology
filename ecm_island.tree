ecm_island url:https://docs.google.com/spreadsheets/d/1OTjfN_lqK-MyfgL3Pf-JuWBg77pnjVNStHWh7Bc8GTg/edit#gid=0 d:"Description of aging-related processes in Extracellular matrix"
	; Клетка находится в Ткани
	org/Cell org/isInside:org/Tissue
	
	; Орган состоит из Тканей
	org/Organ org/consistsOf:org/Tissue
	
	; ВКМ находится между клетками
	ECM a:sbgn/Compartment,org/BiologicalMicroSubstance deabbr:"Extracellular matrix"@en,"Внеклеточный матрикс"@ru org/surroundedBy:org/Cell
	
	; Белки ВКМ находятся в ВКМ
	ECMProtein subclass-of:cell/Protein,_1
		_1 a:owl/Restriction owl/onProperty:sbgn/compartment owl/someValuesFrom:ECM
		
	; Эластин является подклассом Белков ВКМ
	Elastin subclass-of:ECMProtein
	
	; Коллаген является подклассом Белков ВКМ
	Collagen subclass-of:ECMProtein
	
	; Коллаген 1 является подклассом Коллаген
	Collagen_1 a:Collagen
	
	; Внеклеточные визикулы находится в ВКМ
	Extracellular_vesicle a:cell/Vesicle,sbgn/Compartment sbgn/compartment:ECM
	
	; Внеклеточные визикулы содержат молекулы
	ECVMolecule subclass-of:cell/Molecule,_1
		_1 a:owl/Restriction owl/onProperty:sbgn/compartment owl/someValuesFrom:Extracellular_vesicle
		
	; Внеклеточные визикулы содержат мРНК, миРНК, нкРНК, молекулы адгезии, сигнальные молекулы
	ECV_mRNA a:cell/mRNA,ECMMolecule
	ECV_miRNA a:cell/miRNA,ECMMolecule
	ECV_ncRNA a:cell/ncRNA,ECMMolecule
	ECVAdhesionMolecule subclass-of:cell/AdhesionMolecule,ECMMolecule
	ECVSignalMolecule subclass-of:cell/SignalMolecule,ECMMolecule
	
	; некоторые липиды могут входить в состав Внеклеточные визикулы
	ECMLipid subclass-of:cell/Lipid,ECM_Molecule
	
	; поверхность внеклеточной визикулы является компонентом Внеклеточные визикулы
	Extracellular_vesicle_surface eq-class:ECV_surface subclass-of:org/BiologicalSurface,sbgn/Compartment,_1
		_1 a:owl/Restriction owl/onProperty:sbgn/compartment owl/someValuesFrom:Extracellular_vesicle
		
	; входить в состав поверхность внеклеточной визикулы
	ECV_surface_molecule subclass-of:cell/Molecule,_1
		_1 a:owl/Restriction owl/onProperty:sbgn/compartment owl/someValuesFrom:ECV_surface
		
	; некоторые интегрины могут входить в состав поверхность внеклеточной визикулы
	ECV_surface_integrin a:cell/Integrin,ECV_surface_molecule
	
	; CD44 мембранный белковый рецептор могут входить в состав поверхность внеклеточной визикулы
	CD44 subclass-of:cell/MembraneReceptor,ECV_surface_molecule
	ECV_CD44 a:CD44
	
	; HSPG мембранный белковый рецептор могут входить в состав поверхность внеклеточной визикулы
	HSPG subclass-of:cell/MembraneReceptor,ECV_surface_molecule
	ECV_HSPG a:HSPG
	
	; протеазы могут входить в состав поверхность внеклеточной визикулы
	ECV_Protease subclass-of:cell/Protease,ECV_surface_molecule
	
	; фибробласты находится в ВКМ
	cell/Fibroblast subclass_of:ECMMolecule
	
	; 1. сенесцентные фибробласты находится в ВКМ
	; 2. сенесцентные фибробласты усиливают Старение
	SenescentFibroblast subclass-of:gero/SenescentMolecule^2,cell/Fibroblast^1
	
	; неинзиматические сшивки сшивают Коллаген
	; неинзиматические сшивки сшивают Эластин
	Nonenzymatic_crosslinking a:defects/CrossLinking 
	Crosslinked_collagen a:cell/Collagen,sbgn/Complex,defects/DefectiveMolecule
	Crosslinked_elastin a:cell/Elastin,sbgn/Complex,defects/DefectiveMolecule
	Nonenzymatic_crosslinking_molecule a:cell/Molecule sbgn/associatesTo:Crosslinked_collagen,Crosslinked_elastin org/biologicalFunction:Nonenzymatic_crosslinking
	
	; неинзиматические сшивки эластина увеличивается количество старении
	gero/Aging action/increasesConcentration:Crosslinked_elastin
	
	; эпителиальные клетки находятся в ткани
	org/Epithelial_cell sbgn/compartment:org/Epithelial_tissue
	
	; эпителиальные клетки их отделяет ВКМ базальная мембрана
	org/Epithelial_cell org/interleavedBy:org/BasalMembrane
	
	; ММP (металлопротеазы)	является подклассом Белков ВКМ
	MatrixMetalloproteinase subclass-of:ECMProtein
	
	; фибробласты имеет функцию ремоделинга ВКМ
	org/Fibroblast action/remodels:ECM
	
	; фибробласты синтезируют компоненты ВКМ
	ECMComponent subclass-of:_1
		_1 a:owl/Restriction owl/onProperty:sbgn/compartment owl/hasValue:ECM
	org/Fibroblast produces:_1
		_1 a:ECMComponent
	
	; адипоциты находится в ВКМ
	org/Adypocite sbgn/compartment:ECM
	
	; адипоциты находятся в окружении базальной ламиины
	org/Adypocite org/surroundedBy:org/Basal_membrane
	
	; миоэпителиальные клетки контакируют с эпителиальные клетки
	; миоэпителиальные клетки контакируют с базальная мембрана
	org/Myoepithelial_cell org/contacts:org/Epithelial_cell,org/Basal_membrane
	
	; Протеогликаны находится в ВКМ
	; Гликозамингликаны находится в ВКМ
	; Фибринектин находится в ВКМ
	ECM action/contains:_1,_2,Fibronectin
		_1 a:cell/Proteoglycan
		_2 a:cell/Glycosaminoglycan
	
	; эластиновая сеть состоит из эластин, специальные сшивки
	cell/ElastinNetwork a:_1
		_1 a:owl/Restriction owl/onProperty:cell/buildingUnit owl/someValuesFrom:cell/Elastin
	
	; фибринектин увеличивается конценрация при Старение
	; декорин концентрация увеличивается при Старении
	; протеогликаны сумма снижается при Старении
	; эластиновая сеть фрагментируется при Старение
	gero/Aging action/increasesConcentration:cell/Decorin,cell/Fibronectin action/decreasesConcentration:_1 action/fragmentizes:cell/Elastin_network
		_1 a:cell/Proteoglycan
		
	; декорин находится в ВКМ
	cell/Decorin sbgn/compartment:ECM
	
	; гилауроновая кислота находится в ВКМ
	cell/Hyaluronic_acid sbgn/compartment:ECM
